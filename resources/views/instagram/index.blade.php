<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home page</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-md-12 btn btn-danger">
          <div class="col-sm-2">
            <img src="images/{{isset($user->profile_pic) ? $user->profile_pic : 'user.png'}}" height="160" width="160" alt="user_image"/>
          </div>
          @if(isset($user->followers))
            <div class="col-sm-4">Your total followers : {{$user->followers}}</div>
          @endif
          <div class="col-sm-4 text-right">
            <a class="btn btn-primary" href="https://www.instagram.com/oauth/authorize/?client_id=a95fa33fdbc54c0ebac39f3941737459&redirect_uri=http://instagramdemo.com/callback&response_type=code">Authorise</a>
          </div>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-12">
          @if($feeds)
            @foreach($feeds as $feed)
            <div class="col-lg-4 col-md-6 sb-preview text-center">
              <div class="card h-100">
                <div class="sb-preview-img">
                  <img class="card-img-top" src="images/{{$feed->url}}" height="240" width="240" alt="Free Bootstrap Creative Theme - Start Bootstrap">
                </div>
              </div>
            </div>
            @endforeach
          @else
          <div class="col-sm-12 btn btn-primary">
            No Data Found
          </div>
          @endif
        </div>
      </div>
    </div>
  </body>
</html>





