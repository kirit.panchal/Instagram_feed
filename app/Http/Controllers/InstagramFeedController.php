<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Users;
use App\Models\InstaFeed;

class InstagramFeedController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $user = Users::find(1);
        $feeds = array();
        if($user){
            $feeds = InstaFeed::all();
        }
        
        return view('instagram.index', compact('user', 'feeds'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function callback(Request $request) {
        if (isset($request->code)) {
            $client = new Client();
            $response = $client->request('POST', 'https://api.instagram.com/oauth/access_token', [
                'form_params' => [
                    'client_id' => env('INSTAGRAM_CLIENT_ID'),
                    'redirect_uri' => env('INSTAGRAM_CALLBACK'),
                    'code' => $request->code,
                    'grant_type' => 'authorization_code',
                    'client_secret' => env('INSTAGRAM_CLIENT_SECRET'),
                ]
            ]);
            $user_info = json_decode($response->getBody(), true);
            if(isset($user_info)){
                $user = Users::Where('username', $user_info['user']['username'])->first();
                //Get file contents
                $fileContents = file_get_contents($user_info['user']['profile_picture']);
                //Store file into directory
                $ext = pathinfo($user_info['user']['profile_picture'], PATHINFO_EXTENSION);
                file_put_contents('images/'.$user_info['user']['username'].'.'.$ext, $fileContents);
                
                $data = [
                        'name' => $user_info['user']['full_name'],
                        'username' => $user_info['user']['username'],
                        'profile_pic' => $user_info['user']['username'].'.'.$ext,
                        'access_token' => $user_info['access_token'],
                ];
                if($user){
                    $user->update($data);
                }else{
                    Users::create($data);
                }
                $feeds = InstaFeed::getFeeds($user_info['access_token']);
                InstaFeed::store_feeds($feeds);
            }
        }
        return redirect()->route('home');
    }

}
