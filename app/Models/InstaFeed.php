<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;

class InstaFeed extends Model
{
    protected $table = 'insta_feeds';
    protected $fillable = [
        'user_id', 'insta_id', 'code', 'url', 'type','location', 'likes','comments'
    ];
    
    public static function getFeeds($access_token)
    {
        $uri = sprintf('https://api.instagram.com/v1/users/self/media/recent/?access_token=%s', $access_token);
        $client = new Client();
        $response = $client->request('GET', $uri);
        
        if ($response->getStatusCode() === 400) {
            $body = json_decode((string) $response->getBody());

            throw new InstagramException($body->meta->error_message);
        }
        
        return json_decode((string) $response->getBody())->data;
    }
    
    public static function store_feeds($feeds){
        foreach ($feeds as $key => $media) {
                $instaFeed = InstaFeed::where('insta_id', $media->id)->first();
                $fileContents = file_get_contents($media->images->low_resolution->url);
                //Store file into directory
                $ext = pathinfo($media->images->low_resolution->url, PATHINFO_EXTENSION);
                $destination_path = 'public/images/'.$media->id.'.'.$ext;
                file_put_contents($destination_path, $fileContents);
                $user = Users::find(1);
                $data = [
                        'user_id' => $user->id,
                        'insta_id' => $media->id,
                        'url' => $media->id.'.'.$ext,
                        'type' => $media->type,
                        'location' => '',
                        'likes' => $media->likes->count,
                        'comments' => $media->comments->count,
                ];
                if ($instaFeed) {
                    $instaFeed->update($data);
                } else {
                    InstaFeed::create($data);
                }
            }
    }
    
    public static function getUserProfile($access_token){
        $uri = sprintf('https://api.instagram.com/v1/users/self/?access_token=%s', $access_token);
        $client = new Client();
        $response = $client->request('GET', $uri);
        
        if ($response->getStatusCode() === 400) {
            $body = json_decode((string) $response->getBody());

            throw new InstagramException($body->meta->error_message);
        }
        
        return json_decode((string) $response->getBody())->data;
    }
}
