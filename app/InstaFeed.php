<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstaFeed extends Model
{
    protected $table = 'insta_feeds';
    protected $fillable = [
        'user_id', 'insta_id', 'code', 'image', 'location', 'likes','comments'
    ];
}
