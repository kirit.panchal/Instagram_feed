<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\InstaFeed;
use App\Models\Users;

class StoreFeeds extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'storefeeds';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Store User Instagram feeds';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         $users = Users::all();
         foreach ($users as $key => $user) {
            $feeds = InstaFeed::getFeeds($user->access_token);
            InstaFeed::store_feeds($feeds); 
         }
         $this->info('Recent feeds updated successfully');
    }

}
