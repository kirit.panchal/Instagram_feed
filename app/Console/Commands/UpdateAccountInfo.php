<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\InstaFeed;
use App\Models\Users;

class UpdateAccountInfo extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updateuserinfo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update user profile information';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = Users::all();
        foreach ($users as $key => $user) {
            $uri = sprintf('https://api.instagram.com/v1/users/self/?access_token=%s', $user->access_token);
            $userData = InstaFeed::getUserProfile($user->access_token);
            if ($userData) {
                $fileContents = file_get_contents($userData->profile_picture);
                $ext = pathinfo($userData->profile_picture, PATHINFO_EXTENSION);
                //Store file into directory
                file_put_contents(__DIR__ . '/../../../public/images/' . $userData->username . '.' . $ext, $fileContents);
                $user->update([
                    'name' => $userData->full_name,
                    'followers' => $userData->counts->followed_by
                ]);
            }
        }
        $this->info('Users profile updated successfully');
    }

}
